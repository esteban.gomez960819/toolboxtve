 
const defaultState = {
    token: null,
    type:null,
    data:[]
 
};

export  function appInfoReducer(state = defaultState, action) {
    const { type, payload } = action;
    switch (type) {
        case 'SAVE_TOKEN':
            return Object.assign({}, state, {
                token: payload.token,
                type: payload.type,
            });
        case 'SAVE_DATA':
            return Object.assign({}, state, {
                data:[...payload],
            });
         

        default: return state
    }
}