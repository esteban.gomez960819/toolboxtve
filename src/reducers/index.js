import { combineReducers } from "redux";
import {appInfoReducer} from './appInfoReducer'
export default combineReducers({
    appInfoReducer
})