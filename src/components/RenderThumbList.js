import React from 'react'
import { View, Text, FlatList,ImageBackground,StyleSheet, TouchableOpacity ,Alert,SafeAreaView} from 'react-native'
import { useNavigation } from '@react-navigation/native';

export const RenderThumbList = ({item}) => {
    const { items,title} = item
    const navigation = useNavigation() 
    const renderItem =({item})=>{
 
        return(
            <SafeAreaView>
            <TouchableOpacity  onPress={()=>renderVideo(item.videoUrl)} style={styles.containerItem}>
                 <ImageBackground
                    style={styles.imagebackground}
                    source={{
                    uri: `${item.imageUrl}`,
                    }}
                    >
                    </ImageBackground>  
                    <Text style={styles.titelItem}>{item.title}</Text>
                </TouchableOpacity>
                </SafeAreaView>
        )
    }

    const renderVideo=(urlVideo)=>{
        if(urlVideo){
            navigation.navigate("RenderVideo",{urlVideo})
        }else{
            Alert.alert(
                "Video",
                "no disponible",
                [
                  { text: "OK" }
                ])
        }

    }
    return (
        <View style={styles.container}>
             
                <Text style={styles.titlePoster}>{title}</Text>
            
            <FlatList
                horizontal={true}
                data={items}
                renderItem={renderItem} 
            />
        </View>
    )
}

 
const styles = StyleSheet.create({
container:{ height:300,paddingBottom:15},
titlePoster:{fontSize:28,fontWeight: 'bold', padding:10},
containerItem:{width:450, justifyContent:'flex-end',alignItems:'center',height:'100%'},
imagebackground:{margin:5,height:'100%',width:'100%'},
titelItem:{ color:'black',fontWeight: 'bold',fontSize:20,marginBottom:15}
})