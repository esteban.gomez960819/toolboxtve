import React, { useState, useRef } from 'react';
import { View, Text ,StyleSheet} from 'react-native'
 import Video from 'react-native-video';
 
export const RenderVideo = (props) => {

 const {urlVideo} =props.route.params

    const videoPlayer = useRef(null);
    //CONFIGURACIONES DE MULTIMEDIA 
    const [currentTime, setCurrentTime] = useState(0);
    const [duration, setDuration] = useState(0);
    const [isFullScreen, setIsFullScreen] = useState(false);
    const [isLoading, setIsLoading] = useState(true);
    const [paused, setPaused] = useState(false);
 
 
 

    const onProgress = data => {
        // Video Player will continue progress even if the video already ended
        if (!isLoading) {
            setCurrentTime(data.currentTime);
        }
    };

    const onLoad = data => {
        setDuration(data.duration);
        setIsLoading(false);
    };

    const onLoadStart = () => setIsLoading(true);

    const onEnd = () => {
        // Uncomment this line if you choose repeat=false in the video player
        // setPlayerState(PLAYER_STATES.ENDED);
    };

  
    return (
        <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
       <Video 
        source={{ uri: urlVideo, type: 'mpd' }} 
            onEnd={onEnd}
            onLoad={onLoad}
            onLoadStart={onLoadStart}
            onProgress={onProgress}
            paused={paused}
            ref={ref => (videoPlayer.current = ref)}
            resizeMode="cover"
            
            style={{
                position: 'absolute',
                top: 0,
                left: 0,
                bottom: 0,
                right: 0,
            }} />   
            
      
    </View>
    )
}

export default RenderVideo


var styles = StyleSheet.create({
  backgroundVideo: {
    position: 'absolute',
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
  },
});