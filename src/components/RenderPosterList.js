import React from 'react'
import { View, Text, FlatList,ImageBackground,StyleSheet, TouchableOpacity ,Alert,SafeAreaView} from 'react-native'
 import { useNavigation } from '@react-navigation/native';
export const RenderPosterList = ({item}) => {
    const { items,type,title} = item
    const navigation = useNavigation() 

    const renderItem =({item})=>{
        return(
                <TouchableOpacity onPress={()=>renderVideo(item.videoUrl)}>
                    <ImageBackground
                    style={styles.imagebackground}
                    source={{
                        uri:`${item.imageUrl}`,
                    }}
                    >
                        <Text style={styles.titelItem}>{item.title}</Text>
                    </ImageBackground>
            </TouchableOpacity>
        )
    }

    const renderVideo=(urlVideo)=>{
        if(urlVideo){
            navigation.navigate("RenderVideo",{urlVideo})
        }else{
            Alert.alert(
                "Video",
                "no disponible",
                [
                  { text: "OK" }
                ])
        }

    }
    return (
        <SafeAreaView>
        <View style={styles.container}>
                <Text style={styles.titlePoster}>{title}</Text>
            <FlatList
                horizontal={true}
                data={items}
                renderItem={renderItem} 
                keyExtractor={(item,index)=>index.toString()}
            />
        </View>
        </SafeAreaView>
    )
}

 
const styles = StyleSheet.create({
container:{ width:"100%",height:500,paddingBottom:15},
titlePoster:{fontSize:28,fontWeight: 'bold',padding:10},
imagebackground:{ width:250,height: '100%',justifyContent:'flex-end',alignItems:'center',height:'100%',margin:5},
titelItem:{ color:'white',fontWeight: 'bold',fontSize:20,marginBottom:15}
})