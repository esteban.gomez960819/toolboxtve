import React ,{ useEffect, useState }from 'react'
import { View, Text, TouchableOpacity, StyleSheet, FlatList ,SafeAreaView} from 'react-native'
import { useDispatch,useSelector } from "react-redux";
import {getToken,getData} from '../actions/index'
import { RenderPosterList } from '../components/RenderPosterList';
import { RenderThumbList } from '../components/RenderThumbList';
 export const HomeScreen = () => {
     const state = useSelector(state => state.appInfoReducer);
     const [data, setdata] = useState([])
     const dispatch = useDispatch();
   
   
     useEffect(() => {
        dispatch(getToken())

    }, [])
   
   
    useEffect(() => {
        state.token && dispatch(getData(state.token))
    }, [state.token])

    useEffect(() => {
        setdata([...state.data])
    }, [state.data])


    
    const renderItem=(item)=>{
        switch (item.item.type) {
            case 'poster':
                return <RenderPosterList item={item.item}/>
              break 
            case 'thumb':
               return <RenderThumbList item={item.item} />
              break 
    }
}
    return (
        <SafeAreaView>
           <View style={styles.container} >
                <FlatList
                    data={data}
                    renderItem={renderItem}
                    
                    />
           </View>
        </SafeAreaView>
         
    )
}

 const styles = StyleSheet.create({
    container:{
        backgroundColor:'white',
        height:'100%',
        width:'100%'
    },
 })
