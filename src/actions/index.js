 
 
export const setData = (payload,type) => ({
    type,
    payload
});


export const getToken= () =>  {
    return  (dispatch) =>{
   let url= 'https://echo-serv.tbxnet.com/v1/mobile/auth';
   let config={
        method: 'POST', // or 'PUT'
        body: JSON.stringify({ "sub" : "ToolboxMobileTest" }), // data can be `string` or {object}!
        headers:{
        'Content-Type': 'application/json'
        }
    }

    fetch(url,config)
        .then((response) => {
            return response.json();
        })
        .then((datos) => {
           
            const {token,type} =datos
            let payload ={token,type}
            dispatch(setData(payload,'SAVE_TOKEN'))
     
             
        });

    }  
}    
export const getData= (token) =>  {
    return  (dispatch) =>{
   let url= 'https://echo-serv.tbxnet.com/v1/mobile/data';
   let config={
        method: 'GET', 
        headers:{
        'authorization':token
        }
    }

    fetch(url,config)
        .then((response) => {
            return response.json();
        })
        .then((payload) => {
            
            dispatch(setData(payload,'SAVE_DATA'))
           
        });

    }  
}    