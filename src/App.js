import React from 'react'
import { View, Text } from 'react-native'
import { SafeAreaProvider } from 'react-native-safe-area-context';

import { Provider } from 'react-redux'
import { createStore,applyMiddleware } from 'redux'
import { HomeScreen } from './screens/HomeScreen'
import  Reducers from './reducers/index'
import thunk from 'redux-thunk'

import { NavigationContainer } from '@react-navigation/native';
import { Navigator } from './navigation/navigator';

export default  App = () => {
  return (
   <Provider store={createStore(Reducers,applyMiddleware(thunk))}>
    <NavigationContainer>
     <SafeAreaProvider>
      <Navigator/>  
     </SafeAreaProvider>
    </NavigationContainer>
   </Provider>
  )
}
 