import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import { RenderVideo } from '../components/RenderVideo';
import { HomeScreen } from '../screens/HomeScreen';

 
const Stack = createStackNavigator();
export const Navigator = ()=> {
  return (
    <Stack.Navigator
    screenOptions={{
      headerShown:false,
      cardStyle:{
        backgroundColor:'white'
      }
    }}
    >
      <Stack.Screen name="HomeScreen" component={HomeScreen} />
      <Stack.Screen name="RenderVideo" component={RenderVideo} />
    </Stack.Navigator>
  );
}